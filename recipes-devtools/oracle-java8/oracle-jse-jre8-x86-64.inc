PV_UPDATE = "131"
BUILD_NUMBER = "11"

require oracle-jse-jre8.inc

SRC_URI = "http://download.oracle.com/otn-pub/java/jdk/8u${PV_UPDATE}-b${BUILD_NUMBER}/d54c1d3a095b4ff2b6607d096fa80163/jre-8u${PV_UPDATE}-linux-x64.tar.gz"

SRC_URI[md5sum] = "9864b3b90840a2bc4604fba513e87453"
SRC_URI[sha256sum] = "355e5cdb066d4cada1f9f16f358b6fa6280ff5caf7470cf0d5cdd43083408d35"
