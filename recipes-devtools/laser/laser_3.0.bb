SUMMARY = "Horus Vizentec"
DESCRIPTION = "Horus Vizentec"
LICENSE = "CLOSED"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}-${PV}:" 
FILES_${PN} += "${datadir}/java"

SRC_URI += "file://DTFConfiguradorLaser3_0.jar;unpack=0"

do_install(){
	install -m 0755 -d ${D}/usr/share/java
	install -m 0755 ${WORKDIR}/DTFConfiguradorLaser3_0.jar ${D}/usr/share/java
}
