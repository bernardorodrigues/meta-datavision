SUMMARY = "Horus Vizentec"
DESCRIPTION = "Horus Vizentec"
LICENSE = "CLOSED"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}-${PV}:" 
FILES_${PN} += "${datadir}/java"

SRC_URI += "file://horus-1.3.0.jar;unpack=0"
SRC_URI += "file://application.properties;unpack=0"

do_install(){
	install -m 0755 -d ${D}/usr/share/java
	install -m 0755 ${WORKDIR}/horus-1.3.0.jar ${D}/usr/share/java
	install -m 0755 ${WORKDIR}/application.properties ${D}/usr/share/java
}
