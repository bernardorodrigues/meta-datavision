SUMMARY = "Data Vision Vizentec"
DESCRIPTION = "Data Vision Vizentec"
LICENSE = "CLOSED"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}-${PV}:"
FILES_${PN} += "/opt/*"

INSANE_SKIP_${PN} = "already-stripped"

SRC_URI += "file://datavision-4.20.tar.gz"

do_install(){
	install -m 0755 -d ${D}/opt
	cp -r ${WORKDIR}/libs ${D}/opt
	cp -r ${WORKDIR}/banco.sql ${D}/opt
	cp -r ${WORKDIR}/datatraffic ${D}/opt	
}
