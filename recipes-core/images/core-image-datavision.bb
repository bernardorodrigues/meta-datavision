SUMMARY = "Imagem do Data Vision"

LICENSE = "CLOSED"

IMAGE_FEATURES += "splash package-management x11-base ssh-server-dropbear hwcodecs"

inherit core-image

IMAGE_INSTALL_append = " packagegroup-core-x11-sato-datavision mysql5-datavision oracle-jse-jdk7 oracle-jse-jdk8-x86-64 horus datavision freerdp firefox x11vnc vsftpd ntp startup"

#inherit extrausers
#EXTRA_USERS_PARAMS = "usermod -P D@t@Vs789 root;"
