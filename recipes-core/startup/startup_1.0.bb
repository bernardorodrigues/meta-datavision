DESCRIPTION = "Startup scripts"
LICENSE = "CLOSED"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}-${PV}:"
FILES_${PN} += "/etc/*"

SRC_URI = "file://startup-script"

do_install() {
	install -d ${D}${sysconfdir}/init.d
	install -d ${D}${sysconfdir}/rcS.d
	install -m 0755 ${WORKDIR}/startup-script  ${D}/${sysconfdir}/init.d/
	ln -sf ../init.d/startup-script  ${D}/${sysconfdir}/rcS.d/S90startup-script
	echo "cd /opt/datatraffic/datavision/; java7 -jar DTFInstall-1.6.0.jar & disown" >> ${D}/${sysconfdir}/profile
}
