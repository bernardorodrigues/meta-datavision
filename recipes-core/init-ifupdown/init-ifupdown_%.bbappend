FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
FILES_${PN} += "/etc/network/*"

SRC_URI += "file://interfaces"

do_install_append () {
	install -d ${D}${sysconfdir}/network
	install -m 0644 ${WORKDIR}/interfaces  ${D}/${sysconfdir}/network
}
