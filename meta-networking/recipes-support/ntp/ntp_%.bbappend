FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
FILES_${PN} += "/etc/*"

SRC_URI += " \
	file://ntp.conf" 

do_install_append (){
    install -m 0755 ${WORKDIR}/ntp.conf ${D}/${sysconfdir}
}

