FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
FILES_${PN} += "/etc/*"

SRC_URI += " \
	file://vsftpd.conf" 

do_install_append (){
    install -m 0755 ${WORKDIR}/vsftpd.conf ${D}/${sysconfdir}
}
